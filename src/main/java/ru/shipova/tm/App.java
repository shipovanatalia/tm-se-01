package ru.shipova.tm;

import java.util.Scanner;

public class App {
    static Scanner in;

    public static void main(String[] args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");

        ProjectManager projectManager = new ProjectManager();
        TaskManager taskManager = new TaskManager();

        in = new Scanner(System.in);

        while (true) {
            String input = in.nextLine();

            if (input.startsWith("help")) {
                System.out.println("help: Show all commands");
                projectManager.printAllCommands();
                taskManager.printAllCommands();
            } else if (input.startsWith("project")) {
                if ("project-create".equals(input)) {
                    projectManager.create();
                } else if ("project-clear".equals(input)) {
                    projectManager.clear();
                } else if ("project-remove".equals(input)) {
                    projectManager.remove();
                } else if ("project-list".equals(input)) {
                    projectManager.list();
                }
            } else if (input.startsWith("task")) {
                if ("task-create".equals(input)) {
                    taskManager.create();
                } else if ("task-clear".equals(input)) {
                    taskManager.clear();
                } else if ("task-remove".equals(input)) {
                    taskManager.remove();
                } else if ("task-list".equals(input)) {
                    taskManager.list();
                }
            } else {
                System.out.println("Wrong command. Please use command 'help' to get all existing commands.");
            }
        }
    }
}
