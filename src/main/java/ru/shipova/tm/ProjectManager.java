package ru.shipova.tm;

import java.util.*;

public class ProjectManager {
    private Map<String, Project> projectMap;
    private List<String> listOfCommands;

    public ProjectManager() {
        this.projectMap = new HashMap<String, Project>();
        listOfCommands = new ArrayList<String>();
        listOfCommands.add("project-create: Create new project");
        listOfCommands.add("project-list: Show all projects");
        listOfCommands.add("project-clear: Remove all projects");
        listOfCommands.add("project-remove: Remove selected project");
    }

    public void create(){
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");

        String input = App.in.nextLine();
        String id = UUID.randomUUID().toString();
        projectMap.put(id, new Project(id, input));

        System.out.println("[OK]");
    }

    public void list(){
        System.out.println("[PROJECT LIST]");

        Iterator<Map.Entry<String, Project>> iterator = projectMap.entrySet().iterator();
        int i = 1;

        while (iterator.hasNext()) {
            Map.Entry<String, Project> pair = iterator.next();
            String key = pair.getKey();
            System.out.println(i + ": " + key);
            i++;
        }
    }

    public void clear(){
        projectMap.clear();
        System.out.println("[ALL PROJECTS REMOVED]");
    }

    public void remove(){
        System.out.println("ENTER NAME:");
        String input = App.in.nextLine();

        Iterator<Map.Entry<String, Project>> iterator = projectMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Project> entry = iterator.next();
            if (input.equals(entry.getValue().getName())) {
                iterator.remove();
                System.out.println("[PROJECT " + input + " REMOVE]");
            }
        }
    }

    public void printAllCommands(){
        for (String command : listOfCommands) {
            System.out.println(command);
        }
    }
}
